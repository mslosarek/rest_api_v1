var exports,
	connect = require('connect'),
	util = require('util'),
	urlParser = require('url').parse,
	utils = connect.utils,
    inflection = require('inflection'),
    js2xmlparser = require('js2xmlparser'),
    uuid = require('node-uuid'),
    uuidPattern = /^[a-f0-9-]{8}-[a-f0-9-]{4}-[a-f0-9-]{4}-[a-f0-9-]{4}-[a-f0-9-]{12}$/,
	proto = {
		api: {
			version: 1
		}
	},
	app,
	config = {
		authenticationToken: false,
		tables: false,
		connection: false,
		pathPrefix: '/api',
		rootXMLElement: 'root',
		parsers: {
			json: false,
			xml: false,
			txt: false,
			html: false
		}
	},
	parsers = {
		xml: function(responseData) {
			var rootXMLElement = responseData.rootXMLElement || responseData.rootXMLElement || 'root';
			return js2xmlparser(config.rootXMLElement, responseData.data);
		},
		json: function(responseData) {
			return JSON.stringify(responseData);
		},
		jsonp: function(responseData) {
			var callback = responseData.callback || '';
			if (callback) {
				return callback + '(' + JSON.stringify(responseData) + ');';
			} else {
				return parsers.json(responseData);
			}
		}
	},
	isObject = function(obj) {
		return Object.prototype.toString.apply(obj, []) === '[object Object]'
	},
	isArray = function(obj) {
		return Object.prototype.toString.apply(obj, []) === '[object Array]'
	},
	isString = function(obj) {
		return Object.prototype.toString.apply(obj, []) === '[object String]'
	},
	getExtensionString = function() {
		return ':fileExt(?:\.(json|html|xml|txt))?';
	},
	makeUrl = function(str) {
		return (proto.api.urlPrefix + '/' + str + getExtensionString()).replace(/[\/]+/g, '/');
	},
	authHeaders = function(req, res, next) {
		var applicationID = req.header('X-Application-Id'),
			apiKey = req.header('X-Api-Key');

		if (applicationID !== config.applicationID || apiKey !== config.apiKey) {
			respond(req, res, {data: {
				status: 400,
				message: 'Invalid Request'
			}}, 400);
		} else {
			next();
		}
	};



exports = module.exports = setupRestAPIServer;

function setupRestAPIServer(appArgument, configArgument) {
	var pluralRoutes = [];

	// console.log(config);
	utils.merge(config, configArgument);
	app = appArgument;

	if (!config.applicationID) {
		throw(new Error('config.applicationID must be passed to the application'));
	}
	if (!uuidPattern.test(config.applicationID)) {
		throw(new Error('config.applicationID should be a UUID'));
	}

	if (!config.apiKey) {
		throw(new Error('config.apiKey must be passed to the application'));
	}
	if (!uuidPattern.test(config.apiKey)) {
		throw(new Error('config.apiKey should be a UUID'));
	}

	proto.api.urlPrefix = ('/' + config.pathPrefix + '/v' + proto.api.version).replace(/[\/]+/g, '/');

	app.all(makeUrl('/uuid'), function(req, res) {
		var responseData;

		responseData = {
			data: {
				uuid: uuid.v1()
			}
		};

		respond(req, res, responseData);

	});

	app.all(makeUrl('/version'), authHeaders, function(req, res) {
		var responseData;

		responseData = {
			data: {
				version: proto.api.version,
				headers: req.headers
			}
		};

		respond(req, res, responseData);

	});

	if (config.tables) {
		if (isArray(config.tables)) {
			config.tables.forEach(function(tableObject) {
				if (isString(tableObject)) {
					pluralRoutes.push(inflection.pluralize(tableObject));	
				} else if (isObject(tableObject) && tableObject.table) {
					pluralRoutes.push(inflection.pluralize(tableObject.table));	
				}
			});
		} else {
			pluralRoutes.push(inflection.pluralize(config.tables));
		}
	}


	// get records from tables
	app.get(makeUrl('/:pluralRoute(' + pluralRoutes.join('|') + ')/test'), authHeaders, function(req, res) {
		var table = inflection.singularize(req.params['pluralRoute'].toLowerCase()),
			responseData;
		
		responseData = {
			table: table,
			pluralRoute: req.params['pluralRoute'].toLowerCase(),
			rootElement: req.params['pluralRoute'].toLowerCase(),
			data: {
				metadata: {
					msg: 'Should get all records from ' + table,
					resultset: {
						count: false,
						offset: false,
						limit: false
					}
				},
				results: [
					{
						name: "mark"
					},
					{
						name: "mark 2"
					}
				]
			}
		};
		respond(req, res, responseData);
	});

	app.get(proto.api.urlPrefix + '/test', function(req, res) {
		res.json({name: 'Test'});
	});

	utils.merge(app, proto);
	
	return app;
}

function getResponseType(responseData) {
	var responseType = 'json';

	if (responseData.callback) {
		responseType = 'jsonp';
	} else if (responseData.fileExt) {
		if (responseData.fileExt === 'json') {
			responseType = 'json';
		} else if (responseData.fileExt === 'xml') {
			responseType = 'xml';
		} else if (responseData.fileExt === 'html') {
			responseType = 'html';
		} else if (responseData.fileExt === 'txt') {
			responseType = 'text';
		}
	}
	return responseType;
}

function respond(req, res, responseData, statusCode) {
	var responseType,
		parsedUrl = urlParser(req.url, true);

	statusCode = statusCode || 200;

	responseData.parsedUrl = parsedUrl;
	responseData.callback = (parsedUrl.query && parsedUrl.query.callback ? parsedUrl.query.callback : false);
	responseData.fileExt = responseData.fileExt || (req.params['fileExt'] || '').toLowerCase();

	responseData.data = responseData.data || {};
	responseData.rootElement = responseData.rootElement || 'root';

	responseType = getResponseType(responseData);
	
	switch(responseType) {
		case 'jsonp':
			res.statusCode = statusCode;
			res.set('Content-Type', 'application/javascript');
			res.send(parsers.jsonp(responseData));
			break;
		case 'xml':
			res.statusCode = statusCode;
			res.set('Content-Type', 'text/xml');
			res.send(parsers.xml(responseData));
			break;
		case 'txt':
			res.statusCode = statusCode;
			res.set('Content-Type', 'text/plain');
			res.send(parsers.txt(responseData));
			break;
		case 'json':
		default:
			res.statusCode = statusCode;
			res.set('Content-Type', 'application/json');
			res.send(parsers.json(responseData.data));
			break;
	}


}
